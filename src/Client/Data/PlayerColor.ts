export class PlayerColor {
  public static readonly Red: string = 'red';
  public static readonly RedAlt: string = 'pink';
  public static readonly Green: string = 'green';
  public static readonly GreenAlt: string = 'lightgreen';
  public static readonly Blue: string = 'blue';
  public static readonly BlueAlt: string = 'lightblue';
  //TODO: Change
  public static readonly Brown: string = 'brown';
  public static readonly BrownAlt: string = 'lightsalmon';

  public static readonly Colors: string[] = [PlayerColor.Red, PlayerColor.Green, PlayerColor.Blue, PlayerColor.Brown];
}
