import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { SocketClient } from '../../Socket/SocketClient';
import { SetUsernameOutput } from '../Data/DTO/SetUsernameOutput';
import * as SocketEvents from '../../Socket/Data/Static/SocketServerEvents';
import { EmitAcceptUsernameInput } from '../Data/DTO/EmitAcceptUsernameInput';

const LoginContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 50%;
  width: 50%;
  margin: 20px auto;

  .username-input {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  .username-submit {
    width: 100%;
    background-color: #006600;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  .username-submit:hover {
    background-color: #00b300;
    color: black;
  }
`;

const Label = styled.div`
  font-size: 20px;
  margin: auto;
  width: 100%;
  border: 3px solid #006600;
  padding: 10px;
  text-align: center;
  margin: 20px;
`;

const Description = styled.div`
  font-size: 20px;
  margin: auto;
  width: 100%;
  border: 3px solid #006600;
  padding: 10px;
  text-align: center;
  margin: 20px;
  padding: 50px 100px;
`;

const ErrorComponent = styled.div`
  font-size: 20px;
  margin: auto;
  width: 100%;
  border: 3px solid red;
  background-color: pink;
  padding: 10px;
  text-align: center;
  margin: 20px;
  padding: 10px 20px;
`;

type LoginProps = {
  socketClient: SocketClient;
  usernameSubmitted: (username: string) => void;
};

//TODO: Add username validation (middleware)
//TODO: Add logout on server shutdown
//TODO: Add color for user (from server)
//TODO: Make ErrorComponent generic, move socket logic to middleware
const Login: React.FunctionComponent<LoginProps> = ({ socketClient, usernameSubmitted }: LoginProps) => {
  const [username, setUsername] = useState('');
  const [usernameError, setUsernameError] = useState('');

  useEffect(() => {
    socketClient.socket.on(SocketEvents.General.Error, (message: string) => {
      console.log(message);
      setUsernameError(message);
    });
    socketClient.socket.on(SocketEvents.Game.EmitAcceptUsername, (input: EmitAcceptUsernameInput) => {
      socketClient.isUsername = true;
      usernameSubmitted(input.username);
    });
  }, []);

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setUsername(event.target.value);
  };
  const handleUsernameSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    const setUsernameOutput: SetUsernameOutput = {
      username: username,
    };
    socketClient.socket.emit(SocketEvents.Game.SetUsername, setUsernameOutput);
  };

  return (
    <LoginContainer>
      <Label> Pick a username </Label>
      <form onSubmit={handleUsernameSubmit}>
        <input type="text" value={username} className="username-input" onChange={handleUsernameChange} />
        <input type="submit" value="Accept" className="username-submit" />
      </form>
      <Description>
        {' '}
        Type in username you want to use during the game and click Accept. It will be visible to all players.{' '}
      </Description>
      {usernameError && <ErrorComponent>{usernameError}</ErrorComponent>}
    </LoginContainer>
  );
};

export default Login;
