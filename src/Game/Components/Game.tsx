import React, { useState, useEffect } from 'react';
import GameBoard from './GameBoard';
import styled from 'styled-components';
import { MarkingOptions, MarkingOptionsKeys } from '../Data/Types/MarkingOptions';
import RadioButton from '../../Components/Shared/RadioButton';
import ActionButton from '../../Components/Shared/ActionButton';
import * as SocketEvents from '../../Socket/Data/Static/SocketServerEvents';
import { RequestBoardResetOutput } from '../Data/DTO/RequestBoardResetOutput';
import { EmitPlayersInput } from '../Data/DTO/EmitPlayersInput';
import { SocketClient } from '../../Socket/SocketClient';

//TODO: Extract styled components
const GameContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const MarkingOptionsArea = styled.div`
  display: flex;
  flex-direction: row;
  border: 5px solid green;
`;

const PlayersListArea = styled.div`
  display: flex;
  flex-direction: row;
  border: 5px solid violet;
`;

const CardsArea = styled.div`
  border: 5px solid blue;
  padding: 50px 100px;
`;

const BoardArea = styled.div`
  display: flex;
  flex-direction: column;
  border: 5px solid yellow;
`;

const StatusArea = styled.div`
  display: flex;
  flex-direction: row;
  border: 5px solid blue;
`;

const GameBoardWrapper = styled.div`
  background-color: black;
  border: 5px solid red;
`;

const PlayerArea = styled.div`
  border: 5px solid blue;
  padding: 50px;
`;

const PlayerTile = styled.div`
  border: 2px solid green;
  padding: 5px;
  min-width: 10px;
  text-align: center;
`;

type GameProps = {
  socketClient: SocketClient;
  username: string;
};

const Game: React.FunctionComponent<GameProps> = ({ socketClient, username }: GameProps) => {
  const [markingOption, setMarkingOption] = useState(MarkingOptions.Alt);
  const [players, setPlayers] = useState(new Array<string>());

  //TODO: Stop GameBoard re-rendering after making option change
  const handleMarkingOptionChange = (markingOption: string): void => {
    setMarkingOption(markingOption as MarkingOptions);
  };

  const handleMarkingOptionKeyPress = (event: KeyboardEvent): void => {
    if (event.key === MarkingOptionsKeys.Player) {
      setMarkingOption(MarkingOptions.Player);
    } else if (event.key === MarkingOptionsKeys.Alt) {
      setMarkingOption(MarkingOptions.Alt);
    } else if (event.key === MarkingOptionsKeys.Erase) {
      setMarkingOption(MarkingOptions.Erase);
    }
  };

  const handleBoardReset = (): void => {
    const requestBoardResetOutput: RequestBoardResetOutput = {
      boardSizeQ: undefined,
      boardSizeR: undefined,
      voids: undefined,
      rocks: undefined,
    };
    socketClient.socket.emit(SocketEvents.Game.RequestBoardReset, requestBoardResetOutput);
  };

  useEffect(() => {
    document.addEventListener('keydown', handleMarkingOptionKeyPress, false);
    socketClient.socket.on(SocketEvents.Game.EmitPlayers, (clients: EmitPlayersInput[]) => {
      setPlayers(clients.map((client: EmitPlayersInput) => client.name));
    });
  }, []);

  return socketClient && socketClient.isConnected && socketClient.isUsername ? (
    <GameContainer>
      <CardsArea> Card Area </CardsArea>
      <BoardArea>
        <StatusArea>
          <MarkingOptionsArea>
            Marking options:
            <RadioButton
              checked={markingOption === MarkingOptions.Player}
              content={MarkingOptions.Player}
              additionalContent={`( ${MarkingOptionsKeys.Player} )`}
              optionChanged={handleMarkingOptionChange}
            />
            <RadioButton
              checked={markingOption === MarkingOptions.Alt}
              content={MarkingOptions.Alt}
              additionalContent={`( ${MarkingOptionsKeys.Alt} )`}
              optionChanged={handleMarkingOptionChange}
            />
            <RadioButton
              checked={markingOption === MarkingOptions.Erase}
              content={MarkingOptions.Erase}
              additionalContent={`( ${MarkingOptionsKeys.Erase} )`}
              optionChanged={handleMarkingOptionChange}
            />
          </MarkingOptionsArea>
          <PlayersListArea>
            Players:
            <br />
            {players.map((playerName, index) => (
              <PlayerTile key={index}>{playerName !== username ? playerName : `${username} (you)`}</PlayerTile>
            ))}
          </PlayersListArea>
        </StatusArea>
        <GameBoardWrapper>
          <GameBoard socketClient={socketClient} markingOption={markingOption} />
        </GameBoardWrapper>
      </BoardArea>
      <PlayerArea>
        Player area
        <ActionButton content="Generate new board" handleOnClick={handleBoardReset} />
      </PlayerArea>
    </GameContainer>
  ) : (
    //TODO: Add failure component
    <div>Connecting to game...</div>
  );
};

export default Game;
