import React, { useState, useEffect, useReducer } from 'react';
import { Stage, Layer, Shape as ShapeComponent } from 'react-konva';
import { SocketClient } from '../../Socket/SocketClient';
import { HexDrawObject } from '../Entities/HexDrawObject';
import * as SocketEvents from '../../Socket/Data/Static/SocketServerEvents';
import { EmitBoardInput } from '../Data/DTO/EmitBoardInput';
import { EmitHexStatusInput } from '../Data/DTO/EmitHexStatusInput';
import { SetHexStatusOutput } from '../Data/DTO/SetHexStatusOutput';
import { convertBoardToHexDrawObjects, getStatusColor } from '../Util/GameBoardUtil';
import { drawKonvaHex } from '../Util/KonvaUtil';
import { Shape, ShapeConfig } from 'konva/lib/Shape';
import { Context } from 'konva/lib/Context';
import { KonvaEventObject } from 'konva/lib/Node';

type GameBoardProps = {
  socketClient: SocketClient;
  markingOption: string;
};

const GameBoard: React.FunctionComponent<GameBoardProps> = ({ socketClient, markingOption }: GameBoardProps) => {
  const [isLoaded, setIsLoaded] = useState(false);
  //TODO: Clean up reducer - create external dispatcher
  const [boardState, dispatchBoardAction] = useReducer(
    (
      prevBoard: Array<HexDrawObject>,
      {
        action,
        serverBoardData,
        serverHexData,
      }: { action: string; serverBoardData?: Array<HexDrawObject>; serverHexData?: EmitHexStatusInput },
    ) => {
      switch (action) {
        case 'initializeBoardAction':
          if (serverBoardData) {
            return serverBoardData;
          }
          return prevBoard;
        case 'changeHexStatusAction':
          if (serverHexData) {
            const nextBoard = prevBoard.slice();
            const hex = nextBoard.find(
              (localHex) => localHex.coordQ === serverHexData.coordQ && localHex.coordR === serverHexData.coordR,
            );
            if (hex) {
              hex.currentStatus = serverHexData.newStatus;
            }
            return nextBoard;
          }
          return prevBoard;
        default:
          return prevBoard;
      }
    },
    new Array<HexDrawObject>(),
  );

  useEffect(() => {
    socketClient.socket.on(SocketEvents.Game.EmitBoard, (input: EmitBoardInput) => {
      dispatchBoardAction({
        action: 'initializeBoardAction',
        serverBoardData: convertBoardToHexDrawObjects(input),
      });
      setIsLoaded(true);
    });

    socketClient.socket.on(SocketEvents.Game.EmitHexStatus, (input: EmitHexStatusInput) => {
      dispatchBoardAction({ action: 'changeHexStatusAction', serverHexData: input });
    });

    socketClient.socket.emit(SocketEvents.Game.RequestBoard);
  }, []);

  const handleOnMouseDown = (event: KonvaEventObject<MouseEvent>): void => {
    //TODO: If field is already marked as option it was picked e.g. Erase - but field is empty then don't send event
    const { coordQ, coordR } = event.target.attrs;
    const setHexStatusOutput: SetHexStatusOutput = {
      coordQ: coordQ,
      coordR: coordR,
      markingOption: markingOption,
    };
    socketClient.socket.emit(SocketEvents.Game.SetHexStatus, setHexStatusOutput);
  };

  const handleOnMouseOver = (event: KonvaEventObject<MouseEvent>): void => {
    if (event.evt.buttons > 0) {
      handleOnMouseDown(event);
    }
  };

  if (!isLoaded) {
    return <h1> Loading... </h1>;
  }

  return (
    // <Stage width={window.innerWidth * (7.5 / 10)} height={window.innerHeight * (9 / 10)}>
    <Stage width={1300} height={800}>
      <Layer>
        {boardState.map((hex: HexDrawObject, index: number) => (
          //TODO: Change ShapeComponent to Hex (implement Hex component)
          <ShapeComponent
            key={index}
            /* TODO: Custom properties - change to extending Shape */
            coordQ={hex.coordQ}
            coordR={hex.coordR}
            originalStatus={hex.originalStatus}
            currentStatus={hex.currentStatus}
            /* End of custom properties */
            sceneFunc={(context: Context, shape: Shape<ShapeConfig>): void => {
              drawKonvaHex(hex, context, shape);
            }}
            fill={getStatusColor(hex.currentStatus)}
            stroke="black"
            strokeWidth={1}
            onMouseDown={handleOnMouseDown}
            onMouseOver={handleOnMouseOver}
          />
        ))}
      </Layer>
    </Stage>
  );
};

export default GameBoard;
