import { Board } from '../Entities/Board';
import { HexDrawObject } from '../Entities/HexDrawObject';
import { BoardRow } from '../Entities/BoardRow';
import { BoardHex } from '../Entities/BoardHex';
import { HexStatus } from '../Data/Types/HexStatus';
import { HexColor } from '../Data/Types/HexColors';

export function getHexDrawCoords(x: number, y: number, scale: number): Array<number> {
  const coordsArray: Array<number> = [];

  // DISCLAIMER: y axis has opposite value as Y axis is counted from top of window
  coordsArray.push(x - scale * Math.sqrt(3));
  coordsArray.push(y - scale);
  coordsArray.push(x);
  coordsArray.push(y - scale * 2);
  coordsArray.push(x + scale * Math.sqrt(3));
  coordsArray.push(y - scale);
  coordsArray.push(x + scale * Math.sqrt(3));
  coordsArray.push(y + scale);
  coordsArray.push(x);
  coordsArray.push(y + scale * 2);
  coordsArray.push(x - scale * Math.sqrt(3));
  coordsArray.push(y + scale);

  return coordsArray;
}

//TODO: Set scale and starting coord accordingly to window
//TODO: Think about EmitBoardInput <-> Board conversion/mapping
export function convertBoardToHexDrawObjects(board: Board): Array<HexDrawObject> {
  // const scale = (((window.innerWidth * (8 / 10)) / (board.sizeR + 10)) * Math.sqrt(3)) / 6;
  const scale = 20;
  const boardDraw: Array<HexDrawObject> = [];
  board.rows.forEach((row: BoardRow) => {
    row.hexes.forEach((hex: BoardHex) => {
      // const hexCoordX = window.innerWidth / 2 + (row.coordR + 2 * hex.coordQ) * Math.sqrt(3) * scale;
      // const hexCoordY = window.innerHeight / 2 + row.coordR * 3 * scale;
      const hexCoordX = 650 + (row.coordR + 2 * hex.coordQ) * Math.sqrt(3) * scale;
      const hexCoordY = 400 + row.coordR * 3 * scale;
      const drawPointsArray = getHexDrawCoords(hexCoordX, hexCoordY, scale);
      boardDraw.push({
        coordQ: hex.coordQ,
        coordR: row.coordR,
        originalStatus: hex.originalStatus,
        currentStatus: hex.currentStatus,
        points: drawPointsArray,
      });
    });
  });
  return boardDraw;
}

export function getStatusColor(hexStatus: string): string {
  let color = '';
  switch (hexStatus) {
    case HexStatus.Empty: {
      color = HexColor.Empty;
      break;
    }
    case HexStatus.Void: {
      color = HexColor.Void;
      break;
    }
    case HexStatus.Rock: {
      color = HexColor.Rock;
      break;
    }
    default: {
      //TODO: Change to mapping hexStatus to color, Add any checking
      color = hexStatus;
      break;
    }
  }
  return color;
}
