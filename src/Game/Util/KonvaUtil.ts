import { Context } from 'konva/lib/Context';
import { HexDrawObject } from '../Entities/HexDrawObject';
import { Shape, ShapeConfig } from 'konva/lib/Shape';

export function drawKonvaHex(hex: HexDrawObject, context: Context, shape: Shape<ShapeConfig>): void {
  context.beginPath();
  context.moveTo(hex.points[0], hex.points[1]);
  context.lineTo(hex.points[2], hex.points[3]);
  context.lineTo(hex.points[4], hex.points[5]);
  context.lineTo(hex.points[6], hex.points[7]);
  context.lineTo(hex.points[8], hex.points[9]);
  context.lineTo(hex.points[10], hex.points[11]);
  context.closePath();
  context.fillStrokeShape(shape);
}
