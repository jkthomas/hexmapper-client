export class BoardHex {
  coordQ: number;
  currentStatus: string;
  originalStatus: string;

  constructor(coordQ: number, originalStatus = 'e') {
    this.coordQ = coordQ;
    this.originalStatus = originalStatus;
    this.currentStatus = originalStatus;
  }
}
