import { BoardHex } from './BoardHex';

export class BoardRow {
  coordR: number;
  hexes: Array<BoardHex>;

  constructor(coordR: number, coordsQ: Array<BoardHex>) {
    this.coordR = coordR;
    this.hexes = coordsQ;
  }
}
