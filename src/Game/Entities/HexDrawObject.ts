export class HexDrawObject {
  coordQ: number;
  coordR: number;
  originalStatus: string;
  currentStatus: string;
  points: Array<number>;

  constructor(coordQ: number, coordR: number, originalStatus: string, points: Array<number>) {
    this.coordQ = coordQ;
    this.coordR = coordR;
    this.originalStatus = originalStatus;
    this.currentStatus = originalStatus;
    this.points = points;
  }
}
