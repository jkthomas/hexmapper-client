import { BoardRow } from './BoardRow';

export class Board {
  sizeQ: number;
  sizeR: number;
  rows: Array<BoardRow>;

  constructor(sizeQ: number, sizeR: number, rows: Array<BoardRow>) {
    this.sizeQ = sizeQ;
    this.sizeR = sizeR;
    this.rows = rows;
  }

  // TODO: Add encapsulation and make use of this function
  // public changeHexStatus(coordQ: number, coordR: number, isOccupied: boolean): void {
  //   const hex = this.rows.find((row) => row.coordR === coordR)?.hexes.find((hex) => hex.coordQ === coordQ);
  //   if (hex) {
  //     hex.isOccupied = isOccupied;
  //   }
  // }
}
