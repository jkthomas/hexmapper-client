export enum MarkingOptions {
  Player = 'player',
  Alt = 'alt',
  Erase = 'erase',
}

export enum MarkingOptionsKeys {
  Player = 'a',
  Alt = 's',
  Erase = 'd',
}
