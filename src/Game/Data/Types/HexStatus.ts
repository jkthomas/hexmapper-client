export enum HexStatus {
  Empty = 'empty',
  Void = 'void',
  Rock = 'rock',
  Player1 = 'player1',
  Player1Selection = 'player1alt',
  Player2 = 'player2',
  Player2Selection = 'player2alt',
  Player3 = 'player3',
  Player3Selection = 'player3alt',
  Player4 = 'player4',
  Player5Selection = 'player4alt',
}
