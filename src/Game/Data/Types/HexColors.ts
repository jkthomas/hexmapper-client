export enum HexColor {
  Empty = 'white',
  Void = 'black',
  Rock = 'grey',
}
