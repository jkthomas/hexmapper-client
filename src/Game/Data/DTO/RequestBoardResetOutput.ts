export interface RequestBoardResetOutput {
  boardSizeQ?: number;
  boardSizeR?: number;
  voids?: number;
  rocks?: number;
}
