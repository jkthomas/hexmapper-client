interface BoardHex {
  coordQ: number;
  currentStatus: string;
  originalStatus: string;
}

interface BoardRow {
  coordR: number;
  hexes: Array<BoardHex>;
}

export interface EmitBoardInput {
  sizeQ: number;
  sizeR: number;
  rows: Array<BoardRow>;
}
