export interface EmitHexStatusInput {
  newStatus: string;
  coordR: number;
  coordQ: number;
}
