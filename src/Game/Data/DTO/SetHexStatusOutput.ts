export interface SetHexStatusOutput {
  coordQ: number;
  coordR: number;
  markingOption: string;
}
