import { io, Socket } from 'socket.io-client';

export class SocketClient {
  socket: Socket;
  isConnected: boolean = false;
  isUsername: boolean = false;

  constructor(url: string, port: number, path: string) {
    this.socket = io(`${url}:${port}`, {
      path: `/${path}`,
      reconnection: true,
      transports: ['websocket'],
      autoConnect: true,
    });

    this.initializeConnection();
  }

  private initializeConnection(): void {
    this.socket.on('connect', () => {
      console.log(`Connected: ${this.socket.connected}`);
      this.isConnected = this.socket.connected;
    });
  }
}
