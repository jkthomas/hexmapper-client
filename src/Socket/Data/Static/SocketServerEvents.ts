export enum Client {
  Connect = 'connect',
  Disconnect = 'disconnect',
}

export enum Game {
  SetUsername = 'set-username',
  EmitAcceptUsername = 'emit-accept-username',
  RequestPlayers = 'request-players',
  EmitPlayers = 'emit-players',
  RequestBoard = 'request-board',
  EmitBoard = 'emit-board',
  SetHexStatus = 'set-hex-status',
  EmitHexStatus = 'emit-hex-status',
  RequestBoardReset = 'request-board-reset',
}

export enum General {
  Error = 'error-username',
}
