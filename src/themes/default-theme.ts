import { DefaultTheme } from 'styled-components';

const defaultTheme: DefaultTheme = {
  borderRadius: '5px',

  colors: {
    primary: 'red',
    secondary: 'black',
  },
};

export default defaultTheme;
