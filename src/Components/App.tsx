import React, { useState } from 'react';
import './App.css';
import Game from '../Game/Components/Game';
import Login from '../Client/Component/Login';
import { SocketClient } from '../Socket/SocketClient';

const socketClient: SocketClient = new SocketClient('http://localhost', 4000, 'game');

const App: React.FunctionComponent = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [username, setUsername] = useState('');

  const handleUsernameSubmitted = (username: string): void => {
    setUsername(username);
    setIsLoggedIn(true);
  };

  return isLoggedIn ? (
    <Game username={username} socketClient={socketClient} />
  ) : (
    <Login usernameSubmitted={handleUsernameSubmitted} socketClient={socketClient} />
  );
};

export default App;
