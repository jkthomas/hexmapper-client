import React from 'react';
import styled, { css } from 'styled-components';

const RadioButtonWrapper = styled.div`
  width: 50px;
  height: 50px;
  border: 5px solid grey;
  border-color: ${(props: { isChecked: boolean }) => (props.isChecked ? 'black' : 'grey')};
  border-radius: 30%;
  background-color: ${(props: { isChecked: boolean }) => (props.isChecked ? 'grey' : 'black')};
  color: white;
  margin: 10px;
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: default;

  ${(props: { isChecked: boolean }) =>
    !props.isChecked &&
    css`
      &:hover {
        transform: scale(1.15);
        backgroud-color: grey;
        cursor: pointer;
      }
    `}
`;

type RadioButtonProps = {
  checked: boolean;
  content: string;
  additionalContent: string;
  optionChanged: (content: string) => void;
};

const RadioButton: React.FunctionComponent<RadioButtonProps> = ({
  checked,
  content,
  additionalContent,
  optionChanged,
}: RadioButtonProps) => {
  const handleOnClick = (): void => {
    optionChanged(content);
  };

  return (
    <RadioButtonWrapper isChecked={checked} onClick={handleOnClick}>
      {content}
      <br />
      {additionalContent}
    </RadioButtonWrapper>
  );
};

export default RadioButton;
