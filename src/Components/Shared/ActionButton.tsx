import React from 'react';
import styled from 'styled-components';

const ActionButtonWrapper = styled.div`
  width: 150px;
  height: 50px;
  border: 5px solid grey;
  border-radius: 30%;
  background-color: black;
  color: white;
  margin: 10px;
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: default;

  &:hover {
    transform: scale(1.15);
    backgroud-color: grey;
    cursor: pointer;
  }
`;

type ActionButtonProps = {
  content: string;
  handleOnClick: () => void;
};

const ActionButton: React.FunctionComponent<ActionButtonProps> = ({ content, handleOnClick }: ActionButtonProps) => {
  return <ActionButtonWrapper onClick={handleOnClick}>{content}</ActionButtonWrapper>;
};

export default ActionButton;
