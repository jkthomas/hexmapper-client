# HexMapper - client

HexMapper is a websocket-based application for multiplayer hex map interactions. It was made to allow multiple users to edit hex tiles by marking and clearing them with multiple, player-based colors in real time. Created as, but not limited to, board games testing, planning and structurizing tool.

This repository is the client part of the system. Built with `Node v18.15.0`, `React`, `socket.io`, `Konva.js`, `TypeScript` and more.

For the server part of the application, see [HexMapper - server](https://gitlab.com/jkthomas/hexmapper-server) repository.

## Development

To install all of the required packages, run:

```bash
yarn
```

If any of the prerequisites are missing, terminal messages will point you to a sources for download and installation. You can now run the React client or develop the code!

To launch the React client locally, just do:

```bash
yarn start
```

A successful command should result in the logs as below:

![alt text](static/yarn_start_result.png)

As it can be seen from the screenshot above, the default client URL is `http://localhost:3000`. You can access the client app there or using your local IP address.

## State

The current state of the `HexMapper` is **stable, in development**. There are many TODOs including both issues and features, but the overall system is working and can be manually tested locally and when deployed.

Current username picker:

![alt text](static/username_pick_example.png)

Example client view:

![alt text](static/example_client_view.png)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
